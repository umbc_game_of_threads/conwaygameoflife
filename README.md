# Triangular Game of Life

## General Game Rules:

- Cells are in a "dead" state if they have:
    - Less than 2 neighbors or 
    - More than 6 neighbors
- Cells are in a "live" state if they have between 2-5 alive neighbors.


## Virus Rules:
- A virus cell is created if a cell is alive for 10 generations
- If a virus has a neighbor which is a virus, then the neighbor and the virus
will become live cells.
- If a cell is a "virus" for more than 30 generations, it becomes alive.

## Directory Structure:
    /public
        cell.js
        constants.js
        game.js
        game_board.js
        grid.js
        index.html
        main.js
        settings.css
        style.css
    README.md