/*  File: game.js
    Description: This file has the Game class
                which handles the game rules and updates the grid colors.
    SLOC: ##
*/

import { Grid } from "./grid.js";
import { Cell } from "./cell.js";

export class Game {

    constructor() {
        this.genIterations = 0;                 // game generations. clearBoard() sets it to 0
        this.grid = new Grid(GRID_SIZE);        // current generation
        this.nextGrid = new Grid(GRID_SIZE);    // next generation
        this.gamePlaying = false;               // playing state
        this.interval = "";                     // time interval
    }

    getNeighborOffset(tri) {
        /*  Parameters: Triangle Position
            Returns: Constant neighbor offset list matching the position */
        if (tri === TOP) {
            return TOP_TRIANGLE_OFFSET;
        } else if (tri === LEFT) {
            return LEFT_TRIANGLE_OFFSET;
        } else if (tri === BOTTOM) {
            return BOTTOM_TRIANGLE_OFFSET;
        } else if (tri === RIGHT) {
            return RIGHT_TRIANGLE_OFFSET;
        } else {
            console.log("Invalid Parameter in getNeighborOffset(), ", tri)
        }
    };

    getVirusOffset(tri) {
        /* Given a triangle position, return its virus offset. */
        if (tri == TOP) { return TOP_TRI_VIRUS_OFFSET; }
        else if (tri == LEFT) { return LEFT_TRI_VIRUS_OFFSET; }
        else if (tri == BOTTOM) { return BOTTOM_TRI_VIRUS_OFFSET; }
        else if (tri == RIGHT) { return RIGHT_TRI_VIRUS_OFFSET; }
        else { console.log("Invalid Parameter for getVirusOffset()")}
    };

    validNeighbor(row, col) {
        return (row >= 0 && row < GRID_SIZE && col >= 0 && col < GRID_SIZE);
    };

    countLiveNeighbors(row, col, tri) {
        /* Given a triangle's position, return how many live neighbors it has */
        let neighbor_count = 0;
        let offsetList = this.getNeighborOffset(tri);
        for (let offset of offsetList) {
            if (this.validNeighbor(row + offset[0], col + offset[1])) {
                let neighbor = this.grid.grid[row + offset[0]][col + offset[1]][offset[2]];
                if (neighbor.alive && (neighbor.virus === false)) {
                    neighbor_count += 1
                }
            }
        }
        return neighbor_count;
    };

    infectNeighbors(row, col, tri) {
        /*  Given a virus position, infect the neighbors around it
            by setting their state live or dead */
        let virusNeighbors = this.getNeighborOffset(tri);

        for(let neighbor of virusNeighbors) {
           if (this.validNeighbor(row + neighbor[0], col + neighbor[1])) {
               let nextGenNeigh = this.nextGrid.grid[row + neighbor[0]][col + neighbor[1]][neighbor[2]];
               if(nextGenNeigh.temper === false) {
                   let killCell = Math.floor(Math.random() * 3);

                   // If killCell is 1, kill a live cell (do nothing if cell is already dead)
                   if (killCell === 1) {
                       if (nextGenNeigh.alive) {
                           console.log("Randomly chose to kill a live cell.");
                           nextGenNeigh.makeDead();
                           nextGenNeigh.zeroGenLife();
                       } else {
                           console.log("Randomly chose to keep a live cell alive.");
                       }
                   } else {
                       // Otherwise, revive a dead cell (do nothing if cell is already dead)
                       if (nextGenNeigh.alive === false) {
                           console.log("Randomly choose to revive a dead cell");
                           nextGenNeigh.makeAlive();
                           nextGenNeigh.zeroGenLife();
                       } else {
                           // Do nothing if its already a dead cell.
                           console.log("Randomly choose to keep a dead cell dead.")
                       }
                   }
               }
               //Infected neighbors should not be altered by other rules
               nextGenNeigh.temper = true;
           }
        }
    }

    virusCheck(row, col, tri){
        /*  Given an virus' positions, look for neighbors that are viruses.
         *  If a neighbor is also a virus, then the neighbor should become alive. */
      let virusFound = false;
      let offsetList = this.getNeighborOffset(tri); // virus' neighbors

      for (let offset of offsetList) {
          if (this.validNeighbor(row + offset[0], col + offset[1])) {
              let neighbor = this.grid.grid[row + offset[0]][col + offset[1]][offset[2]];
              if (neighbor.virus) {
                  let nextNeigh = this.nextGrid.grid[row + offset[0]][col + offset[1]][offset[2]];
                  virusFound = true;        // found a virus
                  nextNeigh.makeAlive();    // make the virus alive
                  nextNeigh.zeroGenLife();
                  nextNeigh.temper = true;  // prevent the cell from changing by other rules.
              }
          }
      }
      return virusFound;    // Returns if a virus was found.
    }

    computeNextGen() {
        /*  This function computes the next generation by enforcing all the rules of the game.
            It starts with the top left cell and iterates to the bottom right cell of the board.
            This function should only handle cells with the attribute "temper" set to false meaning
            that the cells have not been changed (by virus rules) so normal rules should check them.
            Virus rules will always trump alive/dead rules.
         */

        // Initially copy over all data in the current grid to the next grid.
        for (let r = 0; r < GRID_SIZE; r++) {
            for (let c = 0; c < GRID_SIZE; c++) {
                for (let t = 0; t < 4; t++) {
                    this.nextGrid.grid[r][c][t] = Cell.copy(this.grid.grid[r][c][t]);
                }
            }
        }

        // Iterate over cells again and apply rules:
        for (let r = 0; r < GRID_SIZE; r++) {
            for (let c = 0; c < GRID_SIZE; c++) {
                for (let t = 0; t < 4; t++) {
                    let current_cell = this.grid.grid[r][c][t];
                    let next_gen_cell = this.nextGrid.grid[r][c][t];

                    if (next_gen_cell.temper === false) {

                        // Normal Live/Dead Rules
                        if (current_cell.virus === false) {
                            let neighborCount = this.countLiveNeighbors(r,c,t);

                            // Virus Creation Check:
                            // If live cell is alive for 10 generations, create a virus cell.
                            if (current_cell.alive && current_cell.genLive === VIRUS_CREATION) {
                                let virusOffset = this.getVirusOffset(t);

                                // Make sure virus is not out of bounds
                                if (this.validNeighbor(virusOffset[0], virusOffset[1])) {
                                    let nextGenVirus = this.nextGrid.grid[r + virusOffset[0]][c + virusOffset[1]][virusOffset[2]];
                                    nextGenVirus.makeVirus();   // Set the virus
                                    nextGenVirus.zeroGenLife();
                                    nextGenVirus.temper = true;
                                    // Prevents any other rules from changing this cell change.
                                }
                            }

                            // If too few neighbors or too many neighbors, turn a live cell dead.
                            if ((neighborCount < ALIVE_MIN_THRESHOLD || neighborCount > ALIVE_MAX_THRESHOLD) && current_cell.alive) {
                                next_gen_cell.makeDead();
                                next_gen_cell.zeroGenLife();
                            }
                            // If the correct amount of alive neighbors, turn a dead cell alive.
                            else if (neighborCount >= ALIVE_MIN_THRESHOLD && neighborCount <= ALIVE_MAX_THRESHOLD && current_cell.alive === false) {
                                next_gen_cell.makeAlive();
                                next_gen_cell.zeroGenLife();
                            }
                            // Otherwise, keep a live cell alive or a dead cell dead. Update counter.
                            else {
                                next_gen_cell.addGenLife();
                            }
                        }

                        // Virus Rules
                        else {
                            // If the current cell is a virus, it should first check if it has virus neighbors
                            if (this.virusCheck(r, c, t)) {
                                // If a virus has a virus neighbor, turn the virus alive
                                next_gen_cell.makeAlive();
                                next_gen_cell.zeroGenLife();
                            } else if (current_cell.genLive >= VIRUS_MAX_AGE) {
                                // If a virus is alive for more than 30 generations, make it alive
                                next_gen_cell.makeAlive();
                                next_gen_cell.zeroGenLife();
                                console.log("Virus Cell that has been alive for 30 or " + "more generations coordinates: ", r, c, t);
                                console.log("Virus Cell dies at generationS ", this.genIterations);
                            } else {
                                // If a virus does not become alive, then it should infect neighbors.
                                this.infectNeighbors(r, c, t);
                                next_gen_cell.addGenLife();
                            }
                        }
                    }
                }
            }
        }
    }

    updateGridColorsForNextGen() {
        /* Updates the current grid to the next one and sets the new colors. */
        for (let r = 0; r < GRID_SIZE; r++) {
            for (let c = 0; c < GRID_SIZE; c++) {
                for (let t = 0; t < 4; t++) {
                    // Copy over information.
                    this.grid.grid[r][c][t] = Cell.copy(this.nextGrid.grid[r][c][t]);

                    // Reset the "temper" variables (which prevents changes)
                    let currentCell = this.grid.grid[r][c][t];
                    currentCell.temper = false;
                    this.nextGrid.grid[r][c][t].temper = false;

                    // Update the colors
                    if (currentCell.virus) {
                        document.getElementById(currentCell.svgID).style.fill = cellColorVirus;
                    } else if (currentCell.alive) {
                      document.getElementById(currentCell.svgID).style.fill = cellColorAlive;
                    } else {
                        document.getElementById(currentCell.svgID).style.fill = cellColorDead;
                    }
                }
            }
        }

        // Updates the Report below:
        let cells = this.cellStateCount();
        let liveCells = cells[0];
        let deadCells = cells[1];
        let virusCells = cells[2];
        document.getElementById("numLiveCells").innerHTML = String(liveCells);
        document.getElementById("numDeadCells").innerHTML = String(deadCells);
        document.getElementById("numVirusCells").innerHTML = String(virusCells);
        document.getElementById("genNum").innerHTML = String(this.genIterations);

    }

    stopGame() {
        // Clears the interval, which stops generations from forming.
        clearInterval(this.interval);
    }

    playGame() {
        // Starts the interval and begins to play the game.
        let self = this;
        this.interval = setInterval(function () {
            self.genIterations += 1;
            self.computeNextGen();
            self.updateGridColorsForNextGen();
        }, TIMEOUT_INTERVAL);
    }

    clearBoard() {
        /* Clears the current and next generation grids. Sets all the cells to dead */
        this.grid = new Grid(GRID_SIZE);
        this.nextGrid = new Grid(GRID_SIZE);
        this.genIterations = 0;
        for (let r = 0; r < GRID_SIZE; r++) {
            for (let c = 0; c < GRID_SIZE; c++) {
                for (let t = 0; t < 4; t++) {
                    // Set all cells in game board to dead color.
                    let cell = this.grid.grid[r][c][t];
                    document.getElementById(cell.svgID).style.fill = cellColorDead;
                }
            }
        }
    }

    cellStateCount() {
        /* Counts the number of live, dead, and virus cells in a board */
        let live_cells = 0;
        let dead_cells = 0;
        let virus_cells = 0;

        // Iterates the board and keeps track of live, dead, and virus cells.
        for (let r = 0; r < GRID_SIZE; r++) {
            for (let c = 0; c < GRID_SIZE; c++) {
                for (let t = 0; t < 4; t++) {
                    let currentCell = this.grid.grid[r][c][t];
                    if (currentCell.virus) {
                        virus_cells += 1;
                    } else if (currentCell.alive) {
                        live_cells += 1;
                    } else {
                        dead_cells += 1;
                    }
                }
            }
        }

        // Returns live, dead, and virus cell counts
        return [live_cells, dead_cells, virus_cells];
    }

    randomBoard() {
        /*  Randomly populate the board.
        *   In order to have a more diverse board, the grid will randomly revive/kill every
        *   third cell.
        */
        let randomCount = 0;
        this.clearBoard(); // Clear the board first.
        for (let r = 0; r < GRID_SIZE; r++) {
            for (let c = 0; c < GRID_SIZE; c++) {
                for (let t = 0; t < 4; t++) {
                    if (randomCount === 3) {
                        let currentCell = this.grid.grid[r][c][t];
                        let randomNum = Math.floor((Math.random() * 2));
                        if (randomNum === 1) {
                            currentCell.makeAlive();
                            document.getElementById(currentCell.svgID).style.fill = cellColorAlive;
                        } else {
                            currentCell.makeDead();
                            document.getElementById(currentCell.svgID).style.fill = cellColorDead;
                        }
                        randomCount = 0;
                    }
                    randomCount += 1;
                }
            }
        }
    }
}

