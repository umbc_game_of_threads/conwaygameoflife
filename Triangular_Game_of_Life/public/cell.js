/*
    File: cell.js
    Description: This file contains the Cell Class and methods for altering the Cell object.
    SLOC: 39
 */

'use strict';

export class Cell {
    constructor(id, position) {
        this.svgID = id;        // ID for the SVG object. Format: row#_col#_trianglePosition(spelled out)
        this.temper = false;    // True if the cell has a predetermined state. False if the cell has not been changed.
        this.alive = false;     // State if the cell is alive (true) or dead (false)
        this.virus = false;     // Check if the cell is a virus instead of a normal cell
        this.genLive = 0;       // the number of generations (iterations) a cell is alive or is a virus.
        this.triPos = position  // triPos stands for Triangle Position (TOP, LEFT, RIGHT, BOTTOM in constants.js)
    }

    makeAlive() {
        /* Sets the states for an alive cell */
        this.alive = true;
        this.virus = false;
    }

    makeDead() {
        /* Called when a cell dies. Sets the states for a dead cell.
        *  When a cell dies, iterations it is alive is set to 0.
        */
        this.alive = false;
        this.virus = false;
        this.zeroGenLife(); //
    }

    makeVirus() {
        /* Sets the states for a virus. A virus is not considered a normal cell,
        *  so alive state is set to false
        */
        this.alive = false;
        this.virus = true;
    }

    addGenLife() {
        /* Simply adds to the count of iterations a cell is alive */
        this.genLive += 1;
    }

    zeroGenLife() {
        /* This sets the cell's state of iterations it is alive to 0. This happens when a cell dies.*/
        this.genLive = 0;
    }

    static copy(aCell) {
        /*
        This "copy" function creates a deep copy of a cell. This is used for copying data to the next iteration
        of a board.
         */
        let temp = new Cell(aCell.svgID, aCell.position);
        temp.alive = aCell.alive;
        temp.virus = aCell.virus;
        temp.genLive = aCell.genLive;
        temp.temper = aCell.temper;
        temp.triPos = aCell.triPos;
        return temp;
    }
}
