/* File: game_board.js
 * Code credited to: https://github.com/courupteddata/TriangleGridSvg/
 * Note: The code was altered in order to fit the scope of this project.
 * The github above was used as a starting point.
 * SLOC: 34
 * */

var svg = document.getElementById("theTriangleGrid");

function createTriangle(id, point1, point2, point3) {
  /* This function takes in 3 points (or sides) of a triangle, which are in the format (X,Y).
     This creates the polygon, sets its ID, set the sides to the polygon, and adds it to the SVG object (the grid).
   */

  var tri = document.createElementNS("http://www.w3.org/2000/svg", "polygon");
  tri.setAttribute("id", id);
  let points = String(point1) + " " + String(point2) + " " + String(point3);
  tri.setAttribute("points", points);
  svg.appendChild(tri);
}

function createSquare(centerPointX, centerPointy, id){

  /* This function takes in the center point of a square and its ID.
     This function doesn't actually create a square, but it uses the references points relative to a square.
     All of the triangles have:
     - SQUARE_SIZE as the length of the longest side.
     - the center of a square as the top of their triangle
     Notes: All points are in "X,Y" format.
   */

  const HALF_SIZE = SQUARE_SIZE / 2;  // Height of the Triangles.

  /* svg ID's for each triangle */
  let top_id = id + "_" + TOP;
  let bottom_id = id + "_" + BOTTOM;
  let left_id = id + "_" + LEFT;
  let right_id = id + "_" + RIGHT;

  /* Points of the square as visualized below. All triangles share some of these points */
  let centerPt = String(centerPointX) + "," + String(centerPointy); // all triangles have the center point
  let topLeftPt = String(centerPointX - HALF_SIZE) + "," + String(centerPointy - HALF_SIZE);
  let topRightPt = String(centerPointX + HALF_SIZE) + "," + String(centerPointy - HALF_SIZE);
  let bottomLeftPt = String(centerPointX - HALF_SIZE) + "," + String(centerPointy + HALF_SIZE);
  let bottomRightPt = String(centerPointX + HALF_SIZE) + "," + String(centerPointy + HALF_SIZE);

  /*

     top left        top right
            -----------
            | \  0  / |
            | 1 \ / 3 |
            |   / \   |
            | /  2  \ |
            -----------
    bottom left      bottom right

   */

  // Create the "TOP" Triangle from center point, top left point, and top right point of the square.
  createTriangle(top_id, centerPt, topLeftPt, topRightPt);

  // Create the "BOTTOM" Triangle from the center point, bottom left, and bottom right point of the square.
  createTriangle(bottom_id, centerPt, bottomLeftPt, bottomRightPt);

  // Create the "LEFT" Triangle from the center point, bottom left, and top left point of the square.
  createTriangle(left_id, centerPt, bottomLeftPt, topLeftPt);

  // Create "RIGHT" Triangle from the center point, bottom right, and top right of the square.
  createTriangle(right_id, centerPt, bottomRightPt, topRightPt)
}

export function createGrid() {
  /* Description: Based off of the global, GRID_SIZE, this function creates a grid of "Squares"
  *   SQUARE_SIZE is the length of one side of a "Square". It is how large a square in the grid is on the website.*/

  for(let y = 0; y < GRID_SIZE; y++) {
    for(let x = 0; x < GRID_SIZE; x++) {
      let centerPointX = x * SQUARE_SIZE + SQUARE_SIZE;
      let centerPointy = y * SQUARE_SIZE + SQUARE_SIZE;
      let id = y + "_" + x;
      createSquare(centerPointX, centerPointy, id);
    }
  }
}
