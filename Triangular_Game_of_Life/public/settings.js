/*
    File: settings.js
    Description: This file handles all the "on click" handlers for the settings modal including:
                 Open Settings, Close Settings, Submit Settings.
    SLOC: 33
 */

document.getElementById('settingsButton').addEventListener('click',function(){
    // Settings Button Functionality: Opens the Settings Popup (from clicking settings icon)
    document.getElementById('settingsPopup').style.display = 'flex';
});

document.getElementById('closeButton').addEventListener('click',function(){
    // Close Button Functionality: Close the Settings Popup (from clicking close button)
    document.getElementById('settingsPopup').style.display = 'none';
});

document.getElementById('submitButton').addEventListener('click',function(){
    /* This function grabs the form values from the popup
     * Then, sets all the buttons to those values
     */

    // Sets the colors of the types of cells (alive, dead, virus) from the form.
    cellColorAlive= document.getElementById('liveCellColor').value;
    cellColorDead= document.getElementById('deadCellColor').value;
    cellColorVirus = document.getElementById('virusCellColor').value;


    // Gets the values from the form for background, title, and buttons.
    let backgroundColor = document.getElementById('bgcolor').value;
    let titleColor = document.getElementById('tcolor').value;
    let titleFont = document.getElementById('tfont').value;
    let titleSize = document.getElementById('tsize').value;
    let buttonFont = document.getElementById('bfont').value;
    let buttonBgColor = document.getElementById('bbgcolor').value;
    let buttonFontColor = document.getElementById('bfcolor').value;
    let buttonFontSize = document.getElementById('bfsize').value;


    // Change background color
    document.body.style.backgroundColor = backgroundColor;

    // Change Title Color and Size
    let title = document.getElementById("title");
    title.style.color = titleColor;
    title.style.fontSize = titleSize;
    title.style.fontFamily = titleFont;

    // The customizable buttons: Play/Stop button, Clear button, Get Report Button, and Random Button from main screen
    let buttonListIDs = ['toggleGameButton', 'clearButton', 'reportButton', 'randomButton'];

    // Change the font, color, size, and background color for the customizable buttons.
    for (let i = 0; i < buttonListIDs.length; i++) {
        let button = document.getElementById(buttonListIDs[i]);
        button.style.fontFamily = buttonFont;
        button.style.color = buttonFontColor;
        button.style.fontSize = buttonFontSize;
        button.style.backgroundColor = buttonBgColor;
    }

    // Close the modal when finished.
    document.getElementById('settingsPopup').style.display = 'none';
});
