/*  File: constants.js
    Description: This file contains constants AND variables that are altered by the user.
    Constants include: Offset positions of neighbors and virus cells.
    Variables that the user changes include: cell colors for live, dead, and virus.
    SLOC: 46
 */

var cellColorAlive = "black"; // default color for a live cell (user can change in Settings modal)
var cellColorDead = "white";  // default color for a dead cell (user can change in Settings modal)
var cellColorVirus = "green"; // default color for a virus cell (user can change in Settings modal)

const TIMEOUT_INTERVAL = 2000; // In milliseconds, time between generations.

/* Rules for the Game */
const VIRUS_MAX_AGE = 30;
const VIRUS_CREATION = 10;
const ALIVE_MIN_THRESHOLD = 2;
const ALIVE_MAX_THRESHOLD = 6;

const GRID_SIZE = 20;       // the grid size (the number of rows/columns of "square" cells)
const SQUARE_SIZE = 40;     // The number of pixels a cell should be.

/* Triangle Position Visualization. 0 = TOP, 1 = LEFT, 2 = BOTTOM, 3 = RIGHT
-----------
| \  0  / |
| 1 \ / 3 |
|   / \   |
| /  2  \ |
-----------
 */

const TOP = 0;
const LEFT = 1;
const BOTTOM = 2;
const RIGHT = 3;

/* IMPORTANT NOTE FOR TRIANGLE OFFSETS:
Format is: [row offset, column offset, absolute position for triangle]
*/

/*  Offset Row/Column Visualization for Neighboring "Square" Cells. In the format: (ROW, COLUMN).
    This is just for visualizing purposes.
--------------------------------
| (-1, -1) | (-1, 0) | (-1, 1) |
--------------------------------
| ( 0, -1) | ( 0, 0) | ( 0, 1) |
--------------------------------
| ( 1, -1) | ( 1, 0) | ( 1, 1) |
--------------------------------
 */

/* Offsets for the "top" position triangle */
const TOP_TRIANGLE_OFFSET = [
    [-1, -1, 2], [-1, -1, 3],            // top left row/col neighbor
    [-1, 0, 1], [-1, 0, 2], [-1, 0, 3],  // top row/col neighbor
    [-1, 1, 1], [-1, 1, 2],              // top right row/col neighbor
    [0, -1, 0], [0, -1, 3],              // left row/col neighbor
    [0, 0, 1], [0, 0, 2], [0, 0, 3],     // current row/col
    [0, 1, 0], [0, 1, 1]                 // right row/col neighbor
];

/* Offsets for the "left" position triangle */
const LEFT_TRIANGLE_OFFSET = [
    [-1, -1, 2], [-1, -1, 3],            // top left row/col neighbor
    [-1, 0, 1], [-1, 0, 2],              // top row/col neighbor
    [0, -1, 0], [0, -1, 2], [0, -1, 3],  // left row/col neighbor
    [0, 0, 0], [0, 0, 2], [0, 0, 3],     // current row/col neighbor
    [1, -1, 0], [1, -1, 3],              // bottom left row/col neighbor
    [1, 0, 0], [1, 0, 1]                 // bottom neighbor
];

/* Offsets for the "bottom" position triangle */
const BOTTOM_TRIANGLE_OFFSET = [
    [0, -1, 2], [0, -1, 3],                // left row/col neighbor
    [0, 0, 0], [0, 0, 1], [0, 0, 3],     // current row/col neighbor
    [0, 1, 1], [0, 1, 2],                // right row/col neighbor
    [1, -1, 0], [1, -1, 3],              // bottom left row/col neighbor
    [1, 0, 0], [1, 0, 1], [1, 0, 3],     // bottom row/col neighbor
    [1, 1, 0], [1, 1, 1]                 // bottom right row/col neighbor
];

/* Offsets for the "right" position triangle */
const RIGHT_TRIANGLE_OFFSET = [
    [-1, 0, 2], [-1, 0, 3],              // top row/col neighbor
    [-1, 1, 1], [-1, 1, 2],              // top right row/col neighbor
    [0, 0, 0], [0, 0, 1], [0, 0, 2],     // current row/col neighbor
    [0, 1, 0], [0, 1, 1], [0, 1, 2],     // right row/col neighbor
    [1, 0, 0], [1, 0, 3],                // bottom row/col neighbor
    [1, 1, 0], [1, 1, 1]                 // bottom right row/col neighbor
];

/* Offsets for each triangle for virus */
const TOP_TRI_VIRUS_OFFSET = [1, 0, 0];
const LEFT_TRI_VIRUS_OFFSET = [0, 1, 1];
const BOTTOM_TRI_VIRUS_OFFSET = [-1, 0, 2];
const RIGHT_TRI_VIRUS_OFFSET = [0, -1, 3];


