/*  File: grid.js
    Description: This file contains the Grid class in order to create an object that will hold a 3-D list.
        The 3D list will be indexed as follows: grid[row][column][triangle position]
        In addition, this class will hold the size of the grid.
    SLOC: 18
 */

import { Cell } from './cell.js'; // import the Cell class

export class Grid {
    /*  Description: The Grid is a 3D list. grid[row][col][tri].
    *   Size is the length of the rows/columns of the grid.
    */
    constructor(size) {
        this.grid = [];     //The main column (its index is the row numbers)
        this.size = size;   // length of rows/columns

        for (let i = 0; i < this.size; i++) {

            this.grid.push([]); // Appends columns to the list (its index is the column numbers)

            for (let j = 0; j < this.size; j++) {
                let svgID = String(i) + "_" + String(j); // the svgID is in the format row#_col#

                /* Block below appends a list of the 4 Triangle Cells to each grid[row][column] */
                this.grid[i].push([
                    new Cell(svgID + "_" + String(TOP), TOP),
                    new Cell(svgID + "_" + String(LEFT), LEFT),
                    new Cell(svgID + "_" + String(BOTTOM), BOTTOM),
                    new Cell(svgID + "_" + String(RIGHT), RIGHT)])
            }
        }
    }
}
