/*  File: main.js
    Description: Handles all the clicks a user can do. User can click a button or a cell.
    SLOC: 73
 */

import { Game } from './game.js';               // import Game class
import { createGrid } from './game_board.js';   // import board in order to create it

var mouseDown = false;  // user is not clicking the mouse
let game = new Game();  // creates the game
createGrid();           // creates the SVG grid board
game.randomBoard();     // populates a random board for the

function aliveHandler(svgCell) {
    /* Toggles between live state and dead state for a cell */
    let coordinates = svgCell.id.split("_");
    let row = Number(coordinates[0]);
    let col = Number(coordinates[1]);
    let pos = Number(coordinates[2]);
    let cell = game.grid.grid[row][col][pos];

    if (cell.alive) {
        cell.makeDead();
        cell.zeroGenLife();
        document.getElementById(svgCell.id).style.fill = cellColorDead;
    } else {
        cell.makeAlive();
        cell.zeroGenLife();
        document.getElementById(svgCell.id).style.fill = cellColorAlive;
    }

    /*
    // Uncomment for toggle functionality: Alive, Dead, and Virus
    if (cell.virus) {
        cell.makeAlive();
        cell.zeroGenLife();
        document.getElementById(svgCell.id).style.fill = cellColorAlive;
    } else if (cell.alive) {
        cell.makeDead();
        cell.zeroGenLife();
        document.getElementById(svgCell.id).style.fill = cellColorDead;
    } else {
        cell.makeVirus();
        cell.zeroGenLife();
        document.getElementById(svgCell.id).style.fill = cellColorVirus;
    }
     */
}


function mouseDownHandler() {
    // User pushes down on the mouse.
    mouseDown = true;
    aliveHandler(this);
}

function mouseUpHandler() {
    // User lifts up on the mouse click.
    mouseDown = false;
}

function handleTriDrag() {
    // This is in the event a user clicks and drags
    if (mouseDown) {
        // If the mouse click is pushed down and moving around, handle the clicks.
        aliveHandler(this);
    }
}

function createTriEventHandlers() {
    /* This function creates event handlers (mousedown, mouseover, mouseup) for each polygon. */
    for (let r = 0; r < GRID_SIZE; r++){
        for (let c = 0; c < GRID_SIZE; c++){
            for (let t = 0; t < 4; t++){
                let svgID = String(r) + "_" + String(c) + "_" + String(t);
                document.getElementById(svgID).addEventListener("mousedown", mouseDownHandler);
                document.getElementById(svgID).addEventListener("mouseover", handleTriDrag);
                document.getElementById(svgID).addEventListener("mouseup", mouseUpHandler);
            }
        }
    }
}

document.getElementById('toggleGameButton').addEventListener('click',function(){
    // the game is already playing and the user clicked the "Stop" button
    if (game.gamePlaying) {
        game.gamePlaying = false;   // Stop game state and iterations
        game.stopGame();            // Stops the game
        this.innerHTML = "Play";    // Change the button to "Play"
    } else {
        // If the game is not playing and the user clicked the "Play" button
        game.gamePlaying = true;    // Change the game state
        this.innerHTML = "Stop";    // Change the button to "Stop"
        game.playGame();            // Play the game
    }
});

document.getElementById('clearButton').addEventListener('click',function(){
    // When the user clicks the "Clear" button, the game clears the board.
    game.clearBoard();
});

document.getElementById('reportButton').addEventListener('click',function(){
    // When the user clicks the "Get Report", the report popup becomes visible.
    document.getElementById('reportPopup').style.display = 'flex';
});

document.getElementById('closeReport').addEventListener('click',function(){
    // When the user clicks the "Close" button (in report popup), the popup is no longer visible.
    document.getElementById('reportPopup').style.display = 'none';
});

document.getElementById('randomButton').addEventListener('click',function(){
    // When the user clicks the "Random" button, the game randomly generates a board.
    game.randomBoard();
});

document.getElementById('theTriangleGrid').addEventListener('mouseleave',function(){
    // When the mouse click is down and the mouse goes out of bounds of the board,
    // it no longer registers the mouse clicks.
    mouseDown = false;
});

// Create click events for each triangle.
createTriEventHandlers();
