/* THIS FILES IS FOR RUNNING: NPM START IN ORDER TO USE ELECTRONJS */

const electron = require('electron');
const { app, BrowserWindow } = electron;

let mainWindow;

app.on('ready', () => {
    mainWindow = new BrowserWindow({
        width: 1000,
        height: 700
    });

    mainWindow.setTitle('Triangular Game of Life!');
    mainWindow.loadURL('https://triangular-game-of-life.firebaseapp.com/');

    mainWindow.on('closed', () => {
        mainWindow = null;
    });
});