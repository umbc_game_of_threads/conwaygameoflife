// Settings Button Functionality: Opens the Settings Popup (from clicking settings icon)
document.getElementById('settingsButton').addEventListener('click',function(){
    document.getElementById('settingsPopup').style.display = 'flex';
});

// Close Button Functionality: Close the Settings Popup (from clicking close button)
document.getElementById('closeButton').addEventListener('click',function(){
    document.getElementById('settingsPopup').style.display = 'none';
});

// Submit Button Functionality
document.getElementById('submitButton').addEventListener('click',function(){
    /* This function grabs the form values from the popup
     * Then, sets all the buttons to those values
     */

    cellColorAlive= document.getElementById('liveCellColor').value;
    cellColorDead= document.getElementById('deadCellColor').value;

    // Get Settings from ID
    let backgroundColor = document.getElementById('bgcolor').value;
    let titleColor = document.getElementById('tcolor').value;
    let titleFont = document.getElementById('tfont').value;
    let titleSize = document.getElementById('tsize').value;
    let buttonFont = document.getElementById('bfont').value;
    let buttonBgColor = document.getElementById('bbgcolor').value;
    let buttonFontColor = document.getElementById('bfcolor').value;
    let buttonFontSize = document.getElementById('bfsize').value;


    // Change background color
    document.body.style.backgroundColor = backgroundColor;

    // Change Title Color and Size
    let title = document.getElementById("title");
    title.style.color = titleColor;
    title.style.fontSize = titleSize;
    title.style.fontFamily = titleFont;

    // Change Buttons (Play, stop, clear) color, font, font size, font color
    let buttonListIDs = ['toggleGameButton', 'clearButton', 'reportButton'];

    // The following setting buttons: Choose Font, Choose background color, Choose font size, Choose buttons' background colors, Submit and Close
    for (let i = 0; i < buttonListIDs.length; i++) {
        let button = document.getElementById(buttonListIDs[i]);
        button.style.fontFamily = buttonFont;
        button.style.color = buttonFontColor;
        button.style.fontSize = buttonFontSize;
        button.style.backgroundColor = buttonBgColor;
    }
    document.getElementById('settingsPopup').style.display = 'none';

});
