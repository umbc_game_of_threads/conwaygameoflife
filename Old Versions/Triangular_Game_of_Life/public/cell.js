'use strict';

export class Cell {
    constructor(id, position) {
        this.svgID = id;
        this.position = position;
        this.alive = false;
        this.virus = false;
        this.genLive = 0;
    }

    makeAlive() {
        document.getElementById(this.svgID).color = "black";
        this.alive = true;
        this.virus = false;
    }

    makeDead() {
        this.alive = false;
        this.virus = false;
    }

    makeVirus() {
        this.alive = false;
        this.virus = true;
    }

    addGenLife() {
        this.genLive += 1;
    }

    zeroGenLife() {
        this.genLive = 0;
    }

    static copy(aCell) {
        let temp = new Cell(aCell.svgID, aCell.position);
        temp.alive = aCell.alive;
        temp.virus = aCell.virus;
        temp.genLive = aCell.genLive;
        return temp;
    }
}