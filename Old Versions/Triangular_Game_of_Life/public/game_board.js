/* Code credited to: https://github.com/courupteddata */


var svg = document.getElementById("theTriangleGrid");


function createTriangle(id, point1, point2, point3) {
  var tri = document.createElementNS("http://www.w3.org/2000/svg", "polygon");
  tri.setAttribute("id", id);
  let points = String(point1) + " " + String(point2) + " " + String(point3);
  tri.setAttribute("points", points);
  svg.appendChild(tri);
}

function createSquare(centerPointX, centerPointy, id){
  const HALF_SIZE = SQUARE_SIZE / 2;

  let top_id = id + "_top";
  // Points are in (X,Y) format
  let point1 = String(centerPointX) + "," + String(centerPointy);
  let point2 = String(centerPointX - HALF_SIZE) + "," + String(centerPointy - HALF_SIZE);
  let point3 = String(centerPointX + HALF_SIZE) + "," + String(centerPointy - HALF_SIZE);
  createTriangle(top_id, point1, point2, point3);

  let bottom_id = id + "_bottom";
  point1 = String(centerPointX) + "," + String(centerPointy);
  point2 = String(centerPointX - HALF_SIZE) + "," + String(centerPointy + HALF_SIZE);
  point3 = String(centerPointX + HALF_SIZE) + "," + String(centerPointy + HALF_SIZE);
  createTriangle(bottom_id, point1, point2, point3);

  let left_id = id + "_left";
  point1 = String(centerPointX) + "," + String(centerPointy);
  point2 = String(centerPointX - HALF_SIZE) + "," + String(centerPointy + HALF_SIZE);
  point3 = String(centerPointX - HALF_SIZE) + "," + String(centerPointy - HALF_SIZE);
  createTriangle(left_id, point1, point2, point3);

  let right_id = id + "_right";
  point1 = String(centerPointX) + "," + String(centerPointy);
  point2 = String(centerPointX + HALF_SIZE) + "," + String(centerPointy + HALF_SIZE);
  point3 = String(centerPointX + HALF_SIZE) + "," + String(centerPointy - HALF_SIZE);
  createTriangle(right_id, point1, point2, point3)
}

export function createGrid() {

  for(let y = 0; y < GRID_SIZE; y++) {
    for(let x = 0; x < GRID_SIZE; x++) {
      let centerPointX = x * SQUARE_SIZE + SQUARE_SIZE;
      let centerPointy = y * SQUARE_SIZE + SQUARE_SIZE;
      let id = y + "_" + x;
      createSquare(centerPointX, centerPointy, id);
    }
  }
}
