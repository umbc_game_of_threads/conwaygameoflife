var cellColorAlive = "black";
var cellColorDead = "white";

const GRID_SIZE = 20;
const SQUARE_SIZE = 40;


/* Game Position Visualization
-----------
| \  0  / |
| 1 \ / 3 |
|   / \   |
| /  2  \ |
-----------
 */

const TRI_POS = ['top', 'left', 'bottom', 'right'];

const TOP = 0;
const LEFT = 1;
const BOTTOM = 2;
const RIGHT = 3;


/* Triangle Neighbor Offsets
Note: The triple is: (row offset, col offset, absolute position)
*/

/* Offset Row/Col Visualization for Neighboring Cells
--------------------------------
| (-1, -1) | (-1, 0) | (-1, 1) |
--------------------------------
| ( 0, -1) | ( 0, 0) | ( 0, 1) |
--------------------------------
| ( 1, -1) | ( 1, 0) | ( 1, 1) |
--------------------------------
 */

const TOP_TRIANGLE_OFFSET = [
    [-1, -1, 2], [-1, -1, 3],            // top left row/col neighbor
    [-1, 0, 1], [-1, 0, 2], [-1, 0, 3],  // top row/col neighbor
    [-1, 1, 1], [-1, 1, 2],              // top right row/col neighbor
    [0, -1, 0], [0, -1, 3],              // left row/col neighbor
    [0, 0, 1], [0, 0, 2], [0, 0, 3],     // current row/col
    [0, 1, 0], [0, 1, 1]                 // right row/col neighbor
];

const LEFT_TRIANGLE_OFFSET = [
    [-1, -1, 2], [-1, -1, 3],            // top left row/col neighbor
    [-1, 0, 1], [-1, 0, 2],              // top row/col neighbor
    [0, -1, 0], [0, -1, 2], [0, -1, 3],  // left row/col neighbor
    [0, 0, 0], [0, 0, 2], [0, 0, 3],     // current row/col neighbor
    [1, -1, 0], [1, -1, 3],              // bottom left row/col neighbor
    [1, 0, 0], [1, 0, 1]                 // bottom neighbor
];
/* Offset Row/Col Visualization for Neighboring Cells
--------------------------------
| (-1, -1) | (-1, 0) | (-1, 1) |
--------------------------------
| ( 0, -1) | ( 0, 0) | ( 0, 1) |
--------------------------------
| ( 1, -1) | ( 1, 0) | ( 1, 1) |
--------------------------------
 */
const BOTTOM_TRIANGLE_OFFSET = [
    [0, -1, 2], [0, -1, 3],                // left row/col neighbor
    [0, 0, 0], [0, 0, 1], [0, 0, 3],     // current row/col neighbor
    [0, 1, 1], [0, 1, 2],                // right row/col neighbor
    [1, -1, 0], [1, -1, 3],              // bottom left row/col neighbor
    [1, 0, 0], [1, 0, 1], [1, 0, 3],     // bottom row/col neighbor
    [1, 1, 0], [1, 1, 1]                 // bottom right row/col neighbor
];

const RIGHT_TRIANGLE_OFFSET = [
    [-1, 0, 2], [-1, 0, 3],              // top row/col neighbor
    [-1, 1, 1], [-1, 1, 2],              // top right row/col neighbor
    [0, 0, 0], [0, 0, 1], [0, 0, 2],     // current row/col neighbor
    [0, 1, 0], [0, 1, 1], [0, 1, 2],     // right row/col neighbor
    [1, 0, 0], [1, 0, 3],                // bottom row/col neighbor
    [1, 1, 0], [1, 1, 1]                 // bottom right row/col neighbor
];


/* TODO: Add this back in
const TOP_TRI_VIRUS_OFFSET = [1, 0, 0];
const LEFT_TRI_VIRUS_OFFSET = [0, 1, 1];
const BOTTOM_TRI_VIRUS_OFFSET = [-1, 0, 2];
const RIGHT_TRI_VIRUS_OFFSET = [0, -1, 3];
*/
