import { Game } from './game.js';
import { createGrid } from './game_board.js';

var mouseDown = false;

createGrid(); // creates the SVG grid board
let game = new Game(); //creates the game
game.randomBoard();

function aliveHandler(svgCell) {
    let coordinates = svgCell.id.split("_");
    let row = coordinates[0];
    let col = coordinates[1];

    let pos = 0;
    if (coordinates[2] === "top") {
        pos = 0;
    } else if (coordinates[2] === "left") {
        pos = 1;
    } else if (coordinates[2] === "bottom") {
        pos = 2;
    } else if (coordinates[2] === "right") {
        pos = 3;
    } else {
        console.log("Invalid Triangle Position in handleTriClick(): ", coordinates[2])
    }

    let cell = game.grid.grid[Number(row)][Number(col)][pos];
    if (cell.alive) {
        cell.alive = false;
        document.getElementById(svgCell.id).style.fill = cellColorDead;
    } else {
        cell.alive = true;
        document.getElementById(svgCell.id).style.fill = cellColorAlive;
    }
}


function mouseDownHandler() {
    mouseDown = true;
    aliveHandler(this);
}

function mouseUpHandler() {
    mouseDown = false;
}

function handleTriDrag() {
    if (mouseDown) {
        aliveHandler(this);
    }
}

function createTriEventHandlers() {
    for (let r = 0; r < GRID_SIZE; r++){
        for (let c = 0; c < GRID_SIZE; c++){
            for (let t = 0; t < 4; t++){
                let svgID = String(r) + "_" + String(c) + "_" + TRI_POS[t];
                document.getElementById(svgID).addEventListener("mousedown", mouseDownHandler);
                document.getElementById(svgID).addEventListener("mouseover", handleTriDrag);
                document.getElementById(svgID).addEventListener("mouseup", mouseUpHandler);
            }
        }
    }
}

document.getElementById('toggleGameButton').addEventListener('click',function(){
    // the game is already playing, so the button says "stop", therefore the user clicked the "stop" button
    if (game.gamePlaying) {
        // stop the game
        game.gamePlaying = false;
        game.stopGame();

        // change the button to "play"
        this.innerHTML = "Play";
    } else {
        game.gamePlaying = true;
        this.innerHTML = "Stop";
        game.playGame();
    }
});

document.getElementById('clearButton').addEventListener('click',function(){
    game.clearBoard();
});

document.getElementById('reportButton').addEventListener('click',function(){
    document.getElementById('reportPopup').style.display = 'flex';

    let cells = game.cellStateCount();
    let liveCells = cells[0];
    let deadCells = cells[1];
    let genNum = game.genIterations;

    document.getElementById("numLiveCells").innerHTML = String(liveCells);
    document.getElementById("numDeadCells").innerHTML = String(deadCells);
    document.getElementById("genNum").innerHTML = String(genNum);


});

document.getElementById('closeReport').addEventListener('click',function(){
    document.getElementById('reportPopup').style.display = 'none';
});

document.getElementById('randomButton').addEventListener('click',function(){
    game.randomBoard();
});

document.getElementById('theTriangleGrid').addEventListener('mouseleave',function(){
    mouseDown = false;
});

createTriEventHandlers();
