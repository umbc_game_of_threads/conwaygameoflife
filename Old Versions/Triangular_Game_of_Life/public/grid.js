import { Cell } from './cell.js';

export class Grid {

    /*  Description: The Grid is a 3D list. grid[row][col][tri].
    *   Size is the length of the rows/columns.
    */

    constructor(size) {
        this.grid = []; /* The main column (its index is the row numbers) */
        this.size = size;

        for (let i = 0; i < this.size; i++) {

            /* Appends columns to the list (its index is the column numbers)*/
            this.grid.push([]);

            for (let j = 0; j < this.size; j++) {
                let svgID = String(i) + "_" + String(j);
                /* Appends the list of triangle position in each [row][col] */
                this.grid[i].push([
                    new Cell(svgID + "_top", TOP),
                    new Cell(svgID + "_left", LEFT),
                    new Cell(svgID + "_bottom", BOTTOM),
                    new Cell(svgID + "_right", RIGHT)])
            }
        }
    }
}
