import { Grid } from "./grid.js";
import { Cell } from "./cell.js";


export class Game {

    constructor() {
        this.genIterations = 0;
        this.grid = new Grid(GRID_SIZE);
        this.nextGrid = new Grid(GRID_SIZE);
        this.gamePlaying = false;
        this.interval = "";
    }

    getNeighborOffset(tri) {
        /*
        Parameters: Triangle Position
        Returns: Constant neighbor offset list matching the position
         */
        if (tri === TOP) {
            return TOP_TRIANGLE_OFFSET;
        } else if (tri === LEFT) {
            return LEFT_TRIANGLE_OFFSET;
        } else if (tri === BOTTOM) {
            return BOTTOM_TRIANGLE_OFFSET;
        } else if (tri === RIGHT) {
            return RIGHT_TRIANGLE_OFFSET;
        } else {
            console.log("Invalid Parameter in getNeighborOffset(), ", tri)
        }
    };

    /* TODO: Add this back in
    getVirusOffset(tri) {
        if (tri == TOP) { return TOP_TRI_VIRUS_OFFSET; }
        else if (tri == LEFT) { return LEFT_TRI_VIRUS_OFFSET; }
        else if (tri == BOTTOM) { return BOTTOM_TRI_VIRUS_OFFSET; }
        else if (tri == RIGHT) { return RIGHT_TRI_VIRUS_OFFSET; }
        else { console.log("Invalid Parameter for getVirusOffset()")}
    }
     */

    validNeighbor(row, col) {
        return (row >= 0 && row < GRID_SIZE && col >= 0 && col < GRID_SIZE);
    }

    countLiveNeighbors(row, col, tri) {
        let neighbor_count = 0;
        let offsetList = this.getNeighborOffset(tri);

        for (let offset of offsetList) {
            if (this.validNeighbor(row + offset[0], col + offset[1])) {
                //console.log("Row: ", row, "  Col: ", col, "  Tri: ", tri);
                //console.log(row + offset[0], col + offset[1], offset[2]);
                let neighbor = this.grid.grid[row + offset[0]][col + offset[1]][offset[2]];
                if (neighbor.alive) {
                    neighbor_count += 1
                }
            }
        }

        return neighbor_count;
    };


    /* TODO: Add this back in
    infectNeighbors(row, col, tri) {
        let killCell = true;
        let virusNeighbors = this.getNeighborOffset(tri);
        virusNeighbors.forEach(function(neighbor) {
           if (this.validNeighbor(row + neighbor[0], col + neighbor[1])) {
               let nextGenNeigh = this.nextGrid[row + neighbor[0]][col + neighbor[1]][tri + neighbor[2]];
               if (killCell) {
                   nextGenNeigh.makeDead()
                   killCell = false;
               } else {
                   nextGenNeigh.makeAlive()
                   killCell = true;
               }
           }
        });
    }
     */

    computeNextGen() {
        /*
        Computes the neighbors live/dead status
         */
        for (let r = 0; r < GRID_SIZE; r++) {
            for (let c = 0; c < GRID_SIZE; c++) {
                for (let t = 0; t < 4; t++) {

                    let currentCell = this.grid.grid[r][c][t]; // cell we are looking at

                    this.nextGrid.grid[r][c][t] = Cell.copy(currentCell); //copy over data to new grid
                    let nextGenCell = this.nextGrid.grid[r][c][t];

                    let neighborCount = this.countLiveNeighbors(r, c, t);

                    // if neighbors are less than 6 or greater than 8, kill the cell
                    if (neighborCount < 6 || neighborCount > 8) {
                        if (nextGenCell.alive) {
                            nextGenCell.zeroGenLife();
                            nextGenCell.makeDead();
                        }
                    } else {
                        // otherwise, the cell lives/make the cell alive
                        if (nextGenCell.alive) {
                            nextGenCell.addGenLife();
                        } else {
                            nextGenCell.makeAlive();
                            nextGenCell.zeroGenLife();
                            nextGenCell.addGenLife();
                        }
                    }
                }
            }
        }
    }

    updateGridColorsForNextGen() {
        /*
        Updates this.grid (from this.nextGrid) and sets the colors.
         */
        for (let r = 0; r < GRID_SIZE; r++) {
            for (let c = 0; c < GRID_SIZE; c++) {
                for (let t = 0; t < 4; t++) {
                    this.grid.grid[r][c][t] = Cell.copy(this.nextGrid.grid[r][c][t]);
                    let currentCell = this.grid.grid[r][c][t];
                    if (currentCell.alive) {
                        document.getElementById(currentCell.svgID).style.fill = cellColorAlive;
                    } else {
                        document.getElementById(currentCell.svgID).style.fill = cellColorDead;
                    }
                }
            }
        }
    }

    stopGame() {
        clearInterval(this.interval);
    }

    playGame() {
        let self = this;

        this.interval = setInterval(function () {
            self.genIterations += 1;
            self.computeNextGen();
            self.updateGridColorsForNextGen();
        }, 2000);
    }

    clearBoard() {
        this.grid = new Grid(GRID_SIZE);
        this.nextGrid = new Grid(GRID_SIZE);
        this.genIterations = 0;
        // traverse new gen board and then make changes to
        for (let r = 0; r < GRID_SIZE; r++) {
            for (let c = 0; c < GRID_SIZE; c++) {
                for (let t = 0; t < 4; t++) {
                    let cell = this.grid.grid[r][c][t];
                    document.getElementById(cell.svgID).style.fill = cellColorDead;
                }
            }
        }
    }

    cellStateCount() {
        let live_cells = 0;
        let dead_cells = 0;
        for (let r = 0; r < GRID_SIZE; r++) {
            for (let c = 0; c < GRID_SIZE; c++) {
                for (let t = 0; t < 4; t++) {
                    let currentCell = this.grid.grid[r][c][t];
                    if (currentCell.alive) {
                        live_cells += 1;
                    } else {
                        dead_cells += 1;
                    }
                }
            }
        }
        return [live_cells, dead_cells];
    }

    randomBoard() {
        let randomCount = 0;
        this.clearBoard();
        for (let r = 0; r < GRID_SIZE; r++) {
            for (let c = 0; c < GRID_SIZE; c++) {
                for (let t = 0; t < 4; t++) {
                    if (randomCount === 3) {
                        let currentCell = this.grid.grid[r][c][t];
                        let randomNum = Math.floor((Math.random() * 2));
                        if (randomNum === 1) {
                            currentCell.alive = true;
                            document.getElementById(currentCell.svgID).style.fill = cellColorAlive;
                        } else {
                            currentCell.alive = false;
                            document.getElementById(currentCell.svgID).style.fill = cellColorDead;
                        }
                        randomCount = 0;
                    }
                    randomCount += 1;
                }
            }
        }
    }
}