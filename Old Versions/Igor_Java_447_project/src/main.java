import javax.imageio.ImageIO;
import javax.swing.*;

//import AbsorbColor.MyPanel;

import java.awt.*;
import java.util.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;


public class main extends JFrame implements ActionListener, Runnable{

	public Grid[][] myGrid;
	public JButton start;
	public JButton stop;
	private boolean gameCheck = false;
	
	private int[][] relations = { {1,1} , {0,1} , {1,0} , {-1,-1} , {1,-1} , {-1,1} , {-1,0} , {0,-1} };
	
	public main() {
		
		int dim = Integer.parseInt(JOptionPane.showInputDialog(null,"What is the dimension of the matrix?"));

		
		myGrid = new Grid[dim][dim];
		setLayout(null);
		setSize(600,800);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		start = new JButton("start");
		start.setBounds(180,650,100,50);
		start.addActionListener(this);
		
		stop = new JButton("stop");
		stop.setBounds(290,650,100,50);
		stop.addActionListener(this);
		
		
		JPanel gridPanel = new JPanel();
		gridPanel.setLayout(new GridLayout(dim,dim,2,2));
		gridPanel.setBackground(Color.white);
		gridPanel.setBounds(1,0,585,600);
		
		for(int row = 0; row < dim; row++){
			for(int col = 0; col < dim; col++){
				myGrid[row][col]= new Grid(row,col);
			    BufferedImage image = null;
				try {
					image = ImageIO.read(new File("/Users/roboriot/Documents/OneDrive/UMBC/CLASSES/Junior_SecondSemester/CMSC447/pics/t0.png"));
				} catch (IOException e) {
					e.printStackTrace();
				}
			    JLabel label = new JLabel(new ImageIcon(image));
			    label.setBounds(0, 0, 115, 115);
			    myGrid[row][col].add(label);

				gridPanel.add(myGrid[row][col]);
			}
		}
		
		add(stop);
		add(start);
		add(gridPanel);
		setVisible(true);
		
	}
	

	@Override
	public void run() {
		while(gameCheck) {
			
			int[] counters = {0,0,0,0};
			
			synchronized(myGrid) {
	
				for(int row = 0; row < myGrid.length; row++) {
					for(int col = 0; col < myGrid[0].length; col++) {
						boolean[] check = myGrid[row][col].choice;
						
						if(check[1] == true) {
							counters[0]++;counters[1]++;counters[2]++;counters[3]++;
							
							
						}
						if(check[2] == true) {
							counters[0]++;counters[1]++;counters[2]++;counters[3]++;
							
							

						}
						if(check[3] == true) {
							counters[0]++;counters[1]++;counters[2]++;counters[3]++;
							
							

						}
						if(check[4] == true) {
							counters[0]++;counters[1]++;counters[2]++;counters[3]++;
							
							

						}
					}
				}
			}
		}

	}
	

	public void actionPerformed(ActionEvent e) {
		
		System.out.println(e.getActionCommand());
		
		String act = e.getActionCommand();
		Thread toRun = new Thread(this);
		toRun.start();
		
		if(act == "start") {
			gameCheck = true;
		}
		
		if(act == "stop") {
			
			gameCheck = false;
			
		}
		
	}
	
	
	
	public class Grid extends JPanel implements MouseListener{

		private int row;
		private int col;
		
		private String url = "/Users/roboriot/Documents/OneDrive/UMBC/CLASSES/Junior_SecondSemester/CMSC447/pics/";
		private String[] squares = {url+"t0.png",url+"t1.png",url+"t2.png",url+"t3.png",url+"t4.png"};
		
		private JLabel[] labels = {null, null, null, null, null};
		
		public boolean[] choice = {false,false,false,false,false};
				
		public Grid(int r, int c) {
			
			row = r;
			col = c;
			
			setLayout(null);
			
			for(int i = 0; i < labels.length; i++) {
			    BufferedImage image = null;
				try {
					image = ImageIO.read(new File(squares[i]));
				} catch (IOException e) {
					e.printStackTrace();
				}
			    labels[i] = new JLabel(new ImageIcon(image));
			    labels[i].setBounds(0, 0, 115, 115);
			}
		
			this.addMouseListener(this);
		}
		
		public void addTri(int num) {
			if(num > 0 && num <= 4) {
				add(labels[num]);
				choice[num] = true;
			}
		}
		public void remTri(int num) {
			if(num > 0 && num <= 4) {
				remove(labels[num]);
				choice[num] = false;
			}
		}
		
		public void mouseClicked(MouseEvent e) {
			int myX = e.getX();
			int myY = e.getY();
			
			if(myX >= 27 && myX <= 82 && myY >=5 && myY <= 47) {
				
				if(!choice[1]) {
					System.out.println(choice[1]);
					addTri(1);
					choice[1] = true;
				}
				else {
					remTri(1);
					choice[1] = false;
				}
				
				
			}
			if(myX >= 2 && myX <= 49 && myY >=30 && myY <= 81) {
				if(!choice[2]) { 
					addTri(2);
					choice[2] = true;
				}
				else {
					remTri(2);
					choice[2] = false;
				}
				
			}
			if(myX >= 28 && myX <= 81 && myY >= 62 && myY <= 109) {
				if(!choice[3]) {
					addTri(3);
					choice[3] = true;
				}
				else {
					remTri(3);
					choice[3] = false;
				}
				
			}
			if(myX >= 61 && myX <= 113 && myY >=26 && myY <= 85) {
				if(!choice[4]) { 
					addTri(4);
					choice[4] = true;
				}
				else {
					remTri(4);
					choice[4] = false;
				}
				
			}
				
			repaint();
		}

		public void mousePressed(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseReleased(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseEntered(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseExited(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}
		
		
	}
	
	
	
	
	public static void main(String[] args) {
		
		Thread toWalk = new Thread(new main());
		
//		toWalk.start();
		
//		try {
//			toWalk.join();
//		} catch (InterruptedException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}
		
	}

}
